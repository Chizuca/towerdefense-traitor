﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MouvementPlayer : MonoBehaviour
{
    public KeyCode hautT, basT, gaucheT, droiteT;
    public float speed;
    private float x , y;
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        var angle = (Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg);
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        if (Input.GetKey(hautT))
        {
            transform.Translate(new Vector3(0,0,1) * speed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(basT))
        {
            transform.Translate(new Vector3(0,0,-1) * speed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(gaucheT))
        {
            transform.Translate(new Vector3(-1,0,0) * speed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(droiteT))
        {
            transform.Translate(new Vector3(1,0,0) * speed * Time.deltaTime, Space.World);
        }
    }
}
