﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Mirror;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

[RequireComponent(typeof(CharacterController))]
public class PlayerNetwork : NetworkBehaviour
{
    [SyncVar(hook = "OnLifeChanged")]
    public int lifePoint = 3;

    public Slider barLife;
    public bool canMove = true;
    public float speed;
    private float x , y;
    private CharacterController _controller;

    private Outline_Controller prevController;
    private Outline_Controller currentController;
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        GameManager.AddPlayer(this);
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            HandleLookAtRay();
            //Rotation du player en fonction du Y et X de la souri dans la caméra
            var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
            var angle = (Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg);
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
            //Deplacement player en fonction du monde "Space.world"
            var vert =Input.GetAxis("Vertical");
            var horiz = Input.GetAxis("Horizontal");
            transform.Translate(new Vector3(horiz * speed * Time.deltaTime,0 ,vert * speed * Time.deltaTime),Space.World);
            //Si clique gauche appuyer alors attaquer / utiliser 
            if (Input.GetButtonDown("Fire1"))
            {
                CmdAttack();
            }
        }
        barLife.value = lifePoint;
    }
    private void HandleLookAtRay()
    {
        Ray ray = new Ray(gameObject.transform.position, gameObject.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, 2))
        {
            if (hit.collider.CompareTag("Interact"))
            {
                currentController = hit.collider.GetComponent<Outline_Controller>();
                if (prevController != currentController)
                {
                    HideOutline();
                    ShowOutline();
                }

                prevController = currentController;
            }
            else
            {
               HideOutline(); 
            }
        }
        else
        {
            HideOutline();
        }
    }
    private void OnLifeChanged (int oldLife,int newLife) //coté player client
    {
        if (newLife <= 0)
        {
            //canMove = false;
            GameManager.RemovePlayer(this);
        }
        else
        {
            gameObject.SetActive(true);
        }
    } 
    [Command] //player envoit une action au serveur
    private void CmdAttack()
    {
        RpcAttack();
        Ray ray = new Ray(gameObject.transform.position, gameObject.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit,2))
        {
            Debug.Log(hit.transform.name +" traverse rayon");
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.Log("player");
                PlayerNetwork player = hit.collider.GetComponentInParent<PlayerNetwork>();
                --player.lifePoint;
                if (player.lifePoint <= 0)
                {
                    GameManager.Instance.KillPlayer(player);
                }
            }
        }
    }
    [ClientRpc] //serveur répond au client
    private void RpcAttack()
    {
        //Feedback de tir
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, transform.forward * 2, Color.blue);
    }

    private void ShowOutline()
    {
        if (currentController != null)
        {
            currentController.ShowOutline();
        }
    }
    private void HideOutline()
    {
        if (prevController != null)
        {
            prevController.HideOutline();
            prevController = null;
        }
    }
}
