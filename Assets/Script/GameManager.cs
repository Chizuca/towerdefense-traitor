﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    public List<PlayerNetwork> players = new List<PlayerNetwork>();
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void AddPlayer(PlayerNetwork player)
    {
        Instance.players.Add(player);
    }

    internal void KillPlayer(PlayerNetwork player)
    {
        StartCoroutine(DoRespawnPlayer(player));
        NetworkServer.Destroy(player.gameObject);
    }

    private IEnumerator DoRespawnPlayer(PlayerNetwork player)
    {
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(5f);
        player.gameObject.SetActive(true);
        player.lifePoint = 3;
    }

    public static void RemovePlayer(PlayerNetwork playerNetwork)
    {
        Instance.players.Remove(playerNetwork);
    }
}
